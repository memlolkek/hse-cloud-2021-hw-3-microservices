FROM python:3.9

RUN mkdir /search_app/
WORKDIR /search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY src/services/ src/
COPY data/news_generated.2.csv data/shard_news_generated.csv

ENV PYTHONPATH=./src/
CMD ["python", "src/shard/main.py"]
