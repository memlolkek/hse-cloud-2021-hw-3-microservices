FROM python:3.9

RUN mkdir /search_app/
WORKDIR /search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY src/services/ src/

ENV PYTHONPATH=./src/
CMD ["python", "src/meta_search/main.py"]
