from flask import Flask, request

from common.user.service import UserService
from user.settings import USER_DATA_FILE
from common.data_source import CSV
from user.service import UserServiceImpl
import logging

logger = logging.getLogger(__name__)

class Server(Flask):
    def __init__(self, name: str, user_service: UserService):
        super().__init__(name)
        self._user_service = user_service
        urls = [
            ('/get_user_data', self.get_user_data, {'methods': ['POST']}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def get_user_data(self):
        json = request.get_json()
        logger.error(str(json))
        result = self._user_service.get_user_data(json['user_id'])
        logger.error(result)
        return result

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)


def main():
    server = Server('user', user_service=UserServiceImpl(CSV(USER_DATA_FILE)))
    server.run_server()


if __name__ == '__main__':
    main()
