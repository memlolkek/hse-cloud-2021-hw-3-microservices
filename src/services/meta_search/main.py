from flask import Flask, request
from itsdangerous import json
import requests
import pandas as pd

from common.search.service import BaseSearchService
from common.user.service import UserService
from meta_search.service import MetaSearchService


class Server(Flask):
    def __init__(self, name: str, metasearch: MetaSearchService):
        super().__init__(name)
        self._metasearch = metasearch
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        text = request.args.get('text')
        user_id = int(request.args.get('user_id'))
        sr = self._metasearch.search(text, user_id)
        return {'search_results': sr}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)


class SearchServiceImpl(BaseSearchService):
    def get_search_data(self, search_text, user_data=None, limit=10):
        return pd.DataFrame.from_dict(requests.post(
            'http://search_in_shards:8000/search',
            json={
                'search_text': search_text,
                'user_data': user_data,
                'limit': limit,
            }
        ).json())


class UserServiceImpl(UserService):
    def get_user_data(self, user_id):
        response = requests.post('http://user:8000/get_user_data', json={'user_id': user_id})
        print(response)
        print(response.text)
        return response.json()


def main():
    search = SearchServiceImpl()
    user_service = UserServiceImpl()
    metasearch = MetaSearchService(search, user_service)
    server = Server('metasearch', metasearch=metasearch)
    server.run_server()


if __name__ == "__main__":
    main()
