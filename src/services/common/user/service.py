from abc import abstractmethod

class UserService:
    @abstractmethod
    def get_user_data(self, user_id):
        pass
